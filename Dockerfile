FROM docker.io/library/golang:1.16 as gobuilder

WORKDIR /workspace

COPY go.mod go.mod
COPY wasm/ wasm/

RUN GOOS=js GOARCH=wasm go build -o validate.wasm ./wasm/

FROM docker.io/library/node:15.14-buster as jsbuilder

WORKDIR /workspace

COPY /package.json package.json
COPY /package-lock.json package-lock.json
COPY /workbox-config.js workbox-config.js
COPY /src/ src/
COPY --from=gobuilder /workspace/validate.wasm src/validate.wasm

RUN npm install
RUN npm run build

FROM docker.io/library/nginx:alpine

COPY --from=jsbuilder /workspace/dist /usr/share/nginx/html
COPY --from=jsbuilder /workspace/src/favicon.ico /usr/share/nginx/html/favicon.ico
