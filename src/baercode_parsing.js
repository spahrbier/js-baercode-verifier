/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const keyutil = require('js-crypto-key-utils')
const ec = require('js-crypto-ec')
const fs = require('fs')

const { Certificate } = require('@fidm/x509')

require('bignumber')
const cbor = require('cbor')
const defaults = require('./defaults')
const rfc4648 = require('rfc4648')

const wasmArrayBuffer = fs.readFileSync(__dirname + '/validate.wasm') // eslint-disable-line node/no-path-concat
const defaultTargetURLs = JSON.parse(fs.readFileSync(__dirname + '/target_host.json', 'utf8')) // eslint-disable-line node/no-path-concat
const LAST_UPDATE_KEY = 'bärcodeKeyBundleUpdateTime'

const baercodeParsing = {}

baercodeParsing.triggerKeyBundleFetchCycle = function (force = false, outputSpan) {
    if (this.keyBundleExists() && !force) {
        // Going to try to work offline.
        console.log('Locally stored data exists, going to use that')
    } else {
        console.log('Going to fetch from server')
        this.updateTargetHostAndKeyBundle(outputSpan)
    }
}

baercodeParsing.keyBundleExists = function () {
    const storedKeyBundleTime = window.localStorage.getItem(LAST_UPDATE_KEY)

    if (storedKeyBundleTime !== null) {
        if (this.keyBundleRecent(Date.now(), storedKeyBundleTime)) {
            return true
        }
    }
}

baercodeParsing.coseECDSAPublicKeyToJWK = function (keyData) {
    const decodedCBOR = cbor.decodeFirstSync(keyData, { bigint: true, preferWeb: true })
    const xByte = decodedCBOR.get(-2)
    const yByte = decodedCBOR.get(-3)

    return this.xyValuesToJWK(xByte, yByte)
}

baercodeParsing.xyValuesToJWK = function (xByte, yByte) {
    const xB64 = rfc4648.base64url.stringify(xByte)
    const yB64 = rfc4648.base64url.stringify(yByte)
    return {
        kty: 'EC',
        crv: 'P-521',
        use: 'sig',
        x: xB64,
        y: yB64
    }
}

baercodeParsing.keyBundleOlderThanRecommended = function () {
    const currentTime = new Date(Date.now())
    const keyBundleTime = window.localStorage.getItem(LAST_UPDATE_KEY)
    const maxTimeBeforeRefresh = new Date((keyBundleTime * 1000) + defaults.HOURS_BEFORE_OPTIONAL_REFRESH * 60 * 60 * 1000)

    // If there is no previous key bundle, it'll get automatically downloaded.
    if (keyBundleTime === null) {
        return false
    }

    if (currentTime < maxTimeBeforeRefresh) {
        return false
    } else {
        return true
    }
}

baercodeParsing.keyBundleRecent = function (currentTime, keyBundleTime) {
    const timeNow = new Date(currentTime)
    const maxTimeBeforeRefresh = new Date((keyBundleTime * 1000) + defaults.HOURS_BEFORE_FORCED_REFRESH * 60 * 60 * 1000)

    if (timeNow < maxTimeBeforeRefresh) {
        return true
    } else {
        return false
    }
}

baercodeParsing.updateTargetHostAndKeyBundle = async function (outputSpan) {
    fetch(window.location.origin + '/config/target_host.json', { cache: 'no-store' })
        .then(response => response.json().then(jsonData => {
            const bundleURL = jsonData.bundleURL
            const keyURL = jsonData.keyURL
            const authorityCN = jsonData.authorityCN
            console.log('Got new bundle URL: ' + bundleURL)
            console.log('Got new key URL: ' + keyURL)
            console.log('Got a new authority CN: ' + authorityCN)
            this.updateKeyBundle(bundleURL, keyURL, authorityCN, outputSpan)
        })).catch(error => { // eslint-disable-line node/handle-callback-err
            console.log('Failed fetching target host update, using default: ' + defaultTargetURLs)
            this.updateKeyBundle(defaultTargetURLs.bundleURL, defaultTargetURLs.keyURL, defaultTargetURLs.authorityCN, outputSpan)
        })
}

baercodeParsing.updateKeyBundle = async function (bundleURL, keyURL, authorityCN, outputSpan) {
    await fetch(keyURL, { cache: 'no-store' })
        .then(response => response.text().then(certData => {
            const go = new Go() // eslint-disable-line no-undef
            WebAssembly.instantiate(wasmArrayBuffer, go.importObject).then(result => {
                go.run(result.instance)
                const certificateChainIsValid = window.validateLEChain(certData, authorityCN)
                console.log('Certificate chain is valid?: ' + certificateChainIsValid)

                const baerCodeCert = Certificate.fromPEM(certData)

                if (certificateChainIsValid) {
                    const keyPem = baerCodeCert.publicKey.toPEM()
                    const keyObj = new keyutil.Key('pem', keyPem)

                    keyObj.export('jwk').then(jwkPublicKey => {
                        fetch(bundleURL, { cache: 'no-store' })
                            .then(response => {
                                const reader = response.body.getReader()
                                const contentLength = response.headers.get('Content-Length')
                                let receivedLength = 0
                                let percentage = 0
                                return new ReadableStream({
                                    start (controller) {
                                        return fetchStream()
                                        function fetchStream () {
                                            return reader.read().then(({ done, value }) => {
                                                if (done) {
                                                    setTimeout(function () { $('#progressUpdateModal').modal('hide') }, 750)
                                                    controller.close()
                                                    return
                                                }
                                                receivedLength += value.length
                                                percentage = Math.ceil((receivedLength / contentLength) * 100)
                                                console.log('Updating key bundle: ' + percentage + '% done')
                                                outputSpan.innerText = $.i18n('progress-update') + percentage + '%'
                                                controller.enqueue(value)
                                                return fetchStream()
                                            })
                                        }
                                    }
                                })
                            })
                            .then(stream => new Response(stream))
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error('Network response was not ok')
                                }
                                return response.blob()
                            })
                            .then(keyBundleBlob => {
                                keyBundleBlob.arrayBuffer().then(keyBundleBuffer => {
                                    this.validateAndParseKeyBundle(jwkPublicKey, keyBundleBuffer).then(result => {
                                        const [valid, keyBundleMap] = result

                                        console.log('Is the key bundle signature valid? ' + valid)
                                        console.log(keyBundleMap)
                                        this.storeKeyBundleMap(keyBundleMap)
                                    })
                                })
                            })
                            .catch(error => {
                                console.error('There has been a problem with your fetch operation:', error)
                            })
                    })
                } else {
                    console.log('Invalid certificate')
                }
            })
        })).catch(error => { // eslint-disable-line node/handle-callback-err
            console.log('Error reading key bundle: ' + error)
        })
}

baercodeParsing.validateAndParseKeyBundleCoseKey = async function (coseKeyBuffer, keyBundleBuffer) {
    const jwk = this.coseECDSAPublicKeyToJWK(coseKeyBuffer)
    return this.validateAndParseKeyBundle(jwk, keyBundleBuffer)
}

baercodeParsing.validateAndParseKeyBundle = async function (jwkPublicKey, keyBundleBuffer) {
    const signedBundle = baercodeParsing.uintArrayToCbor(keyBundleBuffer)

    const validSignature = await this.isSignatureValid(signedBundle, jwkPublicKey)
    const bundle = baercodeParsing.decodePayloadFromSignedCbor(signedBundle)
    const bundleMap = baercodeParsing.parseKeyBundleToMap(bundle)

    return [validSignature, bundleMap]
}

baercodeParsing.toArrayBuffer = function (buf) {
    const ab = new ArrayBuffer(buf.length)
    const view = new Uint8Array(ab)
    for (let i = 0; i < buf.length; ++i) {
        view[i] = buf[i]
    }

    return ab
}

baercodeParsing.base64ToVersionAndSignedCbor = function (base64data) {
    const rawCode = new Uint8Array([...atob(base64data)].map(c => c.charCodeAt(0)))
    return this.uintArrayToVersionAndSignedCbor(rawCode)
}

baercodeParsing.hexToVersionAndSignedCbor = function (hexString) {
    const rawCode = this.toArrayBuffer(
        new Uint8Array(hexString.match(/.{1,2}/g).map(b => parseInt(b, 16)))
    )
    return this.uintArrayToVersionAndSignedCbor(rawCode)
}

baercodeParsing.parseBase64EncodedCbor = function (base64data) {
    const rawCode = new Uint8Array([...atob(base64data)].map(c => c.charCodeAt(0)))
    return cbor.decodeFirstSync(rawCode, { bigint: true, preferWeb: true })
}

baercodeParsing.parseHexEncodedCbor = function (hexString) {
    const rawCode = this.toArrayBuffer(
        new Uint8Array(hexString.match(/.{1,2}/g).map(b => parseInt(b, 16)))
    )
    return cbor.decodeFirstSync(rawCode, { bigint: true, preferWeb: true })
}

baercodeParsing.uintArrayToVersionAndSignedCbor = function (rawCode) {
    // Gets our BärCODE version number.
    // it's a little endian uint16.
    const codeVersion = (((rawCode[1] & 0xFF) << 8) | (rawCode[0] & 0xFF))
    const cborEncodedData = rawCode.slice(2)
    const signedCbor = cbor.decodeFirstSync(cborEncodedData, { bigint: true, preferWeb: true })
    return [codeVersion, signedCbor]
}

baercodeParsing.uintArrayToCbor = function (rawCode) {
    const decodedData = cbor.decodeFirstSync(rawCode, { bigint: true, preferWeb: true })
    return decodedData
}

baercodeParsing.isSignatureValidCosePublicKey = async function (signedCbor, keyData) {
    const jwk = this.coseECDSAPublicKeyToJWK(keyData)
    return this.isSignatureValid(signedCbor, jwk)
}

baercodeParsing.isSignatureValidPemPublicKey = async function (signedCbor, keyData) {
    const keyObj = new keyutil.Key('pem', keyData)
    const jwk = await keyObj.export('jwk')
    return this.isSignatureValid(signedCbor, jwk)
}

baercodeParsing.isSignatureValid = async function (signedCbor, jwk) {
    const [prot, unprot, plaintext, signers] = signedCbor.value // eslint-disable-line no-unused-vars
    const protDec = (!prot.length) ? Buffer.alloc(0) : cbor.decodeFirstSync(prot)
    const protDec2 = (!protDec.length) ? Buffer.alloc(0) : cbor.encode(protDec)

    // TODO: Do this a bit better because I used a numerical kid, but that doesn't work here.
    const signer = signers[0]
    const [signerProt, _, signature] = signer // eslint-disable-line no-unused-vars
    const signerProtDec = (!signerProt.length) ? Buffer.alloc(0) : signerProt

    const SigStructure = [
        'Signature',
        protDec2,
        signerProtDec,
        Buffer.alloc(0),
        plaintext
    ]
    const encodedSigStructure = await cbor.encode(SigStructure)
    const valid = await ec.verify(
        encodedSigStructure,
        signature,
        jwk,
        defaults.CURVE_MAPPING[jwk.crv],
        'raw' // input signature is not formatted. DER-encoded signature is available with 'der'.
    )

    return valid
}

baercodeParsing.decryptAndDecodeCbor = async function (encryptedPayload, key) {
    const [eProt, eUnprot, Ciphertext] = encryptedPayload.value

    const aKey = await crypto.subtle.importKey(
        'raw',
        key,
        'AES-GCM',
        true,
        ['encrypt', 'decrypt']
    )

    const EncStructure = [
        'Encrypt0',
        eProt,
        Buffer.alloc(0)
    ]
    const encodedEncStructure = await cbor.encode(EncStructure)

    const IV = eUnprot.get(5)
    const cryptBuf = this.toArrayBuffer(Ciphertext)
    const decryptedData = await crypto.subtle.decrypt(
        {
            name: 'AES-GCM',
            iv: IV,
            additionalData: encodedEncStructure
        },
        aKey,
        cryptBuf
    )

    const decryptedCBOR = await cbor.decode(decryptedData)
    return decryptedCBOR
}

baercodeParsing.getBase64KeyId = function (signedCbor) {
    return btoa(String.fromCharCode.apply(null, signedCbor.value[3][0][1].get(4)))
}

baercodeParsing.validateDecodeAndDecryptSignedCborCoseKeyAes = async function (signedCbor, coseKeyBuffer, aesKey) {
    const jwk = this.coseECDSAPublicKeyToJWK(coseKeyBuffer)
    return this.validateDecodeAndDecryptSignedCbor(signedCbor, jwk, aesKey)
}

baercodeParsing.validateDecodeAndDecryptSignedCborPemKeyAes = async function (signedCbor, pemKeyData, aesKey) {
    const keyObj = new keyutil.Key('pem', pemKeyData)
    const jwk = await keyObj.export('jwk')
    return this.validateDecodeAndDecryptSignedCbor(signedCbor, jwk, aesKey)
}

baercodeParsing.validateDecodeAndDecryptSignedCborKeyData = async function (signedCbor, keyData) {
    const jwk = this.xyValuesToJWK(keyData.x, keyData.y)
    return this.validateDecodeAndDecryptSignedCbor(signedCbor, jwk, keyData.aesKey)
}

baercodeParsing.validateDecodeAndDecryptSignedCbor = async function (signedCbor, jwk, aesKey) {
    const validSignature = await this.isSignatureValid(signedCbor, jwk)
    if (validSignature) {
        const actualCbor = cbor.decodeFirstSync(signedCbor.value[2], { bigint: true, preferWeb: true })
        const decryptedCBOR = await this.decryptAndDecodeCbor(actualCbor, aesKey)

        return [validSignature, decryptedCBOR]
    }
    return [validSignature, null]
}

baercodeParsing.decodePayloadFromSignedCbor = function (signedCbor) {
    return cbor.decodeFirstSync(signedCbor.value[2], { bigint: true, preferWeb: true })
}

baercodeParsing.parseKeyBundleToMap = function (keyBundle) {
    const parsedBundle = {}
    parsedBundle[LAST_UPDATE_KEY] = keyBundle.Date

    keyBundle.Keys.forEach(function (keyArray, index, array) {
        const [credType, aesKey, x, y] = keyArray
        const keyId = x.slice(-16)
        const keyIdBase64 = btoa(String.fromCharCode.apply(null, keyId))

        parsedBundle[keyIdBase64] = {
            credType, aesKey, x, y
        }
    })

    return parsedBundle
}

baercodeParsing.storeKeyBundleMap = function (keyBundleMap) {
    localStorage.clear()
    localStorage.setItem(LAST_UPDATE_KEY, keyBundleMap[LAST_UPDATE_KEY])

    for (const [keyIdBase64, keyData] of Object.entries(keyBundleMap)) {
        // In the local storage we have one single value which is _not_ a key...
        if (keyIdBase64 === LAST_UPDATE_KEY) {
            continue
        }

        const existingData = localStorage.getItem(keyIdBase64)
        const encodedKeyData = this.toLocalStorageFormat(keyData)
        const b64EncodedKeyData = btoa(encodedKeyData)
        let keyArray = []

        if (existingData === 'undefined' || existingData === null) {
            keyArray.push(b64EncodedKeyData)
        } else {
            keyArray = JSON.parse(existingData)
            keyArray.push(b64EncodedKeyData)
        }

        const jsonEncodedData = JSON.stringify(keyArray)
        localStorage.setItem(keyIdBase64, jsonEncodedData)
    }
}

baercodeParsing.toLocalStorageFormat = function (keyData) {
    const data = { ...keyData }
    const b64Mapping = ['aesKey', 'x', 'y']

    b64Mapping.forEach(function (key, index, array) {
        data[key] = btoa(String.fromCharCode.apply(null, data[key]))
    })

    return JSON.stringify(data)
}

baercodeParsing.fromLocalStorageFormat = function (encodedData) {
    const parsedData = JSON.parse(encodedData)
    const b64Mapping = ['aesKey', 'x', 'y']
    b64Mapping.forEach(function (key, index, array) {
        parsedData[key] = new Uint8Array([...atob(parsedData[key])].map(c => c.charCodeAt(0)))
    })
    return parsedData
}

baercodeParsing.getKeyDataFromStorage = function (keyIdBase64) {
    const jsonEncodedData = localStorage.getItem(keyIdBase64)
    if (jsonEncodedData) {
        const b64KeyArray = JSON.parse(jsonEncodedData)
        const keyDataArray = b64KeyArray.map(keyData => this.fromLocalStorageFormat(atob(keyData)))
        return [true, keyDataArray]
    } else {
        return [false, null]
    }
}

module.exports = baercodeParsing
