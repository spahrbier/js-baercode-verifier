/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require('jsdom-global')()

const assert = require('assert')
const defaults = require('../src/defaults')
const verifierConfiguration = require('../src/verifier_configuration')
describe('Verifier configuration', function () {
    describe('parseAndValidateConfig()', function () {
        it('should return a config object when passed proper json string configuration', function () {
            const testData = {
                accepted_procedures: [1, 2, 3],
                antigen_max_age: 24,
                pcr_max_age: 72,
                vaccine_min_age: 2,
                vaccine_max_age: 52,
                recovered_min_age_days: 28,
                recovered_max_age_days: 180,
                play_sound: false
            }
            const jsonString = JSON.stringify(testData)

            const [valid, config, errors] = verifierConfiguration.parseAndValidateConfig(jsonString)
            assert.strictEqual(valid, true)
            assert.deepStrictEqual(config, testData)
            assert.strictEqual(errors, null)
        })
    })

    describe('validateConfig()', function () {
        const testData = {
            accepted_procedures: [1, 2, 3],
            antigen_max_age: 24,
            pcr_max_age: 72,
            vaccine_min_age: 2,
            vaccine_max_age: 52,
            recovered_min_age_days: 28,
            recovered_max_age_days: 180,
            play_sound: false
        }

        it('should return a config object when passed proper configuration', function () {
            const [valid, config, errors] = verifierConfiguration.validateConfig(testData)
            assert.strictEqual(valid, true)
            assert.deepStrictEqual(config, testData)
            assert.strictEqual(errors, null)
        })

        it('should return an error saying required items are missing', function () {
            for (const [key, value] of Object.entries(testData)) { // eslint-disable-line no-unused-vars
                const data = { ...testData }
                delete data[key]
                const [valid, config, errors] = verifierConfiguration.validateConfig(data)
                assert.strictEqual(valid, false)
                assert.deepStrictEqual(config, null)
                assert.strictEqual(errors[0].keyword, 'required')
            }
        })

        it('should return an error saying what is wrong with the value', function () {
            const testexpectations = {
                accepted_procedures: [
                    {
                        value: [0],
                        keyword: 'minimum',
                        message: 'should be >= 1'
                    },
                    {
                        value: [8],
                        keyword: 'maximum',
                        message: 'should be <= 4'
                    }
                ],
                antigen_max_age: [
                    {
                        value: -1,
                        keyword: 'minimum',
                        message: 'should be >= 0'
                    },
                    {
                        value: 25,
                        keyword: 'maximum',
                        message: 'should be <= 24'
                    }
                ],
                pcr_max_age: [
                    {
                        value: -1,
                        keyword: 'minimum',
                        message: 'should be >= 0'
                    },
                    {
                        value: 73,
                        keyword: 'maximum',
                        message: 'should be <= 72'
                    }
                ],
                vaccine_min_age: [
                    {
                        value: 1,
                        keyword: 'minimum',
                        message: 'should be >= 2'
                    },
                    {
                        value: 53,
                        keyword: 'maximum',
                        message: 'should be <= 52'
                    }
                ],
                vaccine_max_age: [
                    {
                        value: 1,
                        keyword: 'minimum',
                        message: 'should be >= 2'
                    },
                    {
                        value: 53,
                        keyword: 'maximum',
                        message: 'should be <= 52'
                    }
                ],
                recovered_min_age_days: [
                    {
                        value: 1,
                        keyword: 'minimum',
                        message: 'should be >= 28'
                    },
                    {
                        value: 181,
                        keyword: 'maximum',
                        message: 'should be <= 180'
                    }
                ],
                recovered_max_age_days: [
                    {
                        value: 1,
                        keyword: 'minimum',
                        message: 'should be >= 28'
                    },
                    {
                        value: 181,
                        keyword: 'maximum',
                        message: 'should be <= 180'
                    }
                ]
            }

            for (const [configName, testList] of Object.entries(testexpectations)) {
                testList.forEach(function (testInput, index, array) {
                    const data = { ...testData }
                    data[configName] = testInput.value
                    const [valid, config, errors] = verifierConfiguration.validateConfig(data) // eslint-disable-line no-unused-vars
                    assert.strictEqual(errors[0].keyword, testInput.keyword)
                    assert.strictEqual(errors[0].message, testInput.message)
                })
            }
        })
    })

    describe('getAcceptedProcedures()', function () {
        it('should translate config accepted procedures to actual procedures', () => {
            const testData = [
                {
                    config: [1],
                    expected: [1]
                },
                {
                    config: [2],
                    expected: [2]
                },
                {
                    config: [3],
                    expected: [3, 4, 5, 6, 7]
                },
                {
                    config: [1, 2],
                    expected: [1, 2]
                },
                {
                    config: [1, 2, 3],
                    expected: [1, 2, 3, 4, 5, 6, 7]
                },
                {
                    config: [2, 3],
                    expected: [2, 3, 4, 5, 6, 7]
                }
            ]
            testData.forEach(td => {
                const config = { ...defaults.DEFAULT_CONFIG }
                config.accepted_procedures = td.config
                const acceptedProcedures = verifierConfiguration.getAcceptedProcedures(config)

                assert.strictEqual(acceptedProcedures.length, td.expected.length)
                td.expected.forEach(expected => {
                    assert.strictEqual(acceptedProcedures.includes(expected), true)
                })
            })
        })
    })
})
