/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

const fs = require('fs')
const Ajv = require('ajv')
const ajv = new Ajv()

const defaults = {}

const baercodeSchema = JSON.parse(fs.readFileSync(__dirname + '/baercode_schema.json', 'utf8')) // eslint-disable-line node/no-path-concat
const configSchema = JSON.parse(fs.readFileSync(__dirname + '/config_schema.json', 'utf8')) // eslint-disable-line node/no-path-concat

defaults.baercodeSchemaValidator = ajv.compile(baercodeSchema)
defaults.configSchemaValidator = ajv.compile(configSchema)

// Note: The accepted procedures are
// 1: Antigen
// 2: PCR
// 3: Vaccinations
// 4: Recovered
defaults.DEFAULT_CONFIG = {
    accepted_procedures: [1, 2, 3, 4],
    antigen_max_age: 24,
    pcr_max_age: 72,
    vaccine_min_age: 2,
    vaccine_max_age: 52,
    recovered_min_age_days: 28,
    recovered_max_age_days: 180,
    play_sound: true
}

defaults.PROCEDURE_MAPPINGS = {
    1: ['antigen_max_age'],
    2: ['pcr_max_age'],
    3: ['vaccine_min_age', 'vaccine_max_age'],
    4: ['recovered_min_age_days', 'recovered_max_age_days']
}

defaults.PROCEDURE_REQUIRED_COUNT = {
    1: 1,
    2: 1,
    3: 2,
    4: 1,
    5: 2,
    6: 2,
    7: 1
}

defaults.PROCEDURE_I18N_MAPPINGS = {
    antigen_max_age: 'antigen-config-label',
    pcr_max_age: 'pcr-config-label',
    vaccine_min_age: 'vaccine-min-age-label',
    vaccine_max_age: 'vaccine-max-age-label',
    recovered_min_age_days: 'recovered-min-age-days-label',
    recovered_max_age_days: 'recovered-max-age-days-label'
}

defaults.AVAILABLE_PROCEDURES = {
    1: 'antigen',
    2: 'pcr',
    3: 'cormirnaty',
    4: 'janssen',
    5: 'moderna',
    6: 'vaxzevria',
    7: 'recovered'
}

defaults.AVAILABLE_PROCEDURES_I18N = {
    1: 'antigen-i18n',
    2: 'pcr-i18n',
    3: 'vaccine-i18n',
    4: 'recovered-i18n'
}

// ['Placeholder value', 'Antigen test', 'PCR test', 'Vaccine']

defaults.CREDENTIAL_TYPE = {
    1: 'vaccine',
    2: 'test'
}

defaults.TYPE_OF_PROCEDURE = {
    test: [1, 2],
    vaccine: [3, 4, 5, 6, 7]
}

defaults.RECOVERED_PROCEDURE = 7

defaults.CURVE_MAPPING = {
    'P-256': 'SHA-256',
    'P-384': 'SHA-384',
    'P-521': 'SHA-512'
}

defaults.VERIFIER_LINK = 'https://scan.baercode.de'

defaults.HOURS_BEFORE_FORCED_REFRESH = 24
defaults.HOURS_BEFORE_OPTIONAL_REFRESH = 12

defaults.SUBJECT = 'authority.baercode.de'

module.exports = defaults
