/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

const defaults = require('./defaults')
const verifierConfiguration = require('./verifier_configuration')

const baercodeVerifier = {}

baercodeVerifier.validateBaerCode = function (baerCode) {
    const baerCodeCopy = { ...baerCode }
    baerCodeCopy.procedures = [...baerCode.procedures]
    baerCodeCopy.date_of_birth = baerCodeCopy.date_of_birth.toISOString()
    baerCodeCopy.procedures.forEach((element, index) => {
        baerCodeCopy.procedures[index] = {
            type: element.type,
            time: element.time.toISOString()
        }
    })
    const valid = defaults.baercodeSchemaValidator(baerCodeCopy)
    return [valid, valid ? baerCode : null, defaults.baercodeSchemaValidator.errors]
}

baercodeVerifier.cborToBaerCode = function (cborData) {
    const procedures = cborData[4].sort((a, b) => b[1] - a[1]).map(p => {
        const procedure = {
            type: p[0],
            time: new Date(p[1] * 1000)
        }
        return procedure
    }
    )

    const baercode = {
        first_name: cborData[0],
        last_name: cborData[1],
        date_of_birth: new Date(cborData[2] * 1000),
        disease_type: cborData[3],
        procedures: procedures,
        procedure_operator: cborData[5],
        procedure_result: cborData[6]
    }

    return this.validateBaerCode(baercode)
}

baercodeVerifier.parseAndValidateBaerCode = function (jsonString) {
    return this.validateBaerCode(JSON.parse(jsonString))
}

baercodeVerifier.checkConfigAndBaerCode = function (configuration, baerCode) {
    const [validConfig, processedConfig, configErrors] = verifierConfiguration.validateConfig(configuration) // eslint-disable-line no-unused-vars
    const [validBaerCode, processedBaerCode, baerCodeErrors] = this.validateBaerCode(baerCode) // eslint-disable-line no-unused-vars
    const configAndBaerCode = {
        config: processedConfig,
        baercode: processedBaerCode
    }

    if (!validConfig) {
        console.log('Error when validating configuration: ' + configErrors)
        return [false, configAndBaerCode, 'invalid-configuration']
    }

    if (!validBaerCode) {
        console.log('Errors when validating BärCODE: ' + baerCodeErrors)
        return [false, configAndBaerCode, 'invalid-baercode']
    }

    return [true, configAndBaerCode, null]
}

baercodeVerifier.procedureAllowed = function (acceptedProcedures, procedure) {
    if (acceptedProcedures.includes(procedure)) {
        return [true, 'procedure-allowed']
    } else {
        return [false, 'procedure-not-allowed']
    }
}

baercodeVerifier.vaccineWithinInterval = function (currentTime, minAgeWeeks, maxAgeWeeks, procedureTime) {
    const latestValid = new Date(procedureTime.getTime() + maxAgeWeeks * 7 * 24 * 60 * 60 * 1000)
    const earliestValid = new Date(procedureTime.getTime() + minAgeWeeks * 7 * 24 * 60 * 60 * 1000)

    if (procedureTime > currentTime) {
        return [false, 'procedure-time-in-future']
    } else if (earliestValid > currentTime) {
        return [false, 'procedure-age-below-minimum']
    } else if (latestValid < currentTime) {
        return [false, 'procedure-age-above-maximum']
    } else {
        return [true, 'procedure-age-within-range']
    }
}

baercodeVerifier.negativeTestAgeValid = function (currentTime, maxAgeHours, procedureTime) {
    const maxAge = new Date(currentTime.getTime() - maxAgeHours * 60 * 60 * 1000)

    if (procedureTime > currentTime) {
        return [false, 'procedure-time-in-future']
    } else if (maxAge < procedureTime && procedureTime < currentTime) {
        return [true, 'procedure-age-valid']
    } else {
        return [false, 'procedure-validity-expired']
    }
}

baercodeVerifier.positivePcrTestAgeValid = function (currentTime, procedureTime) {
    const minAgeMillis = defaults.DEFAULT_CONFIG.recovered_min_age_days * 24 * 60 * 60 * 1000
    const maxAgeMillis = defaults.DEFAULT_CONFIG.recovered_max_age_days * 24 * 60 * 60 * 1000
    const minAge = new Date(currentTime.getTime() - minAgeMillis)
    const maxAge = new Date(currentTime.getTime() - maxAgeMillis)

    if (procedureTime > currentTime) {
        return [false, 'procedure-time-in-future']
    } else if (minAge > procedureTime && maxAge < procedureTime && procedureTime < currentTime) {
        return [true, 'procedure-age-valid']
    } else {
        return [false, 'procedure-validity-expired']
    }
}

baercodeVerifier.getEarliestProcedure = function (baerCode) {
    return baerCode.procedures.sort((a, b) => a.time.getTime() - b.time.getTime())[0]
}

baercodeVerifier.getLatestProcedure = function (baerCode) {
    return baerCode.procedures.sort((a, b) => b.time.getTime() - a.time.getTime())[0]
}

baercodeVerifier.isRecoveredAndVaccinated = function (baerCode) {
    const earliestProcedure = this.getEarliestProcedure(baerCode)
    const latestProcedure = this.getLatestProcedure(baerCode)
    const excludeRecovered = defaults.TYPE_OF_PROCEDURE.vaccine.filter(x => x !== defaults.RECOVERED_PROCEDURE)

    if (earliestProcedure.type === defaults.RECOVERED_PROCEDURE && excludeRecovered.includes(latestProcedure.type)) {
        return [true, 'is-recovered-and-vaccinated']
    } else {
        return [false, 'is-not-recovered-and-vaccinated']
    }
}

baercodeVerifier.baerCodeValid = function (currentTime, configuration, credType, baerCode) {
    let [valid, configAndBaerCode, error] = this.checkConfigAndBaerCode(configuration, baerCode)
    if (!valid) {
        return [valid, error]
    }

    const procedure = this.getLatestProcedure(configAndBaerCode.baercode);
    [valid, error] = this.procedureAllowed(
        verifierConfiguration.getAcceptedProcedures(configAndBaerCode.config),
        procedure.type
    )
    if (!valid) {
        return [valid, error]
    }

    [valid, error] = this.hasMatchingCredentialType(credType, procedure.type)
    if (!valid) {
        // This should not happen unless intregrators made a mistake, or we are using testing credentials.
        // Logging BärCODE as debug info if we are running in browser.
        if (typeof fetch !== 'undefined') {
            console.log(baerCode)
        }

        return [valid, error]
    }

    [valid, error] = this.isRecoveredAndVaccinated(baerCode)
    if (valid) {
        return this.vaccineWithinInterval(
            currentTime,
            configAndBaerCode.config.vaccine_min_age,
            configAndBaerCode.config.vaccine_max_age,
            procedure.time)
    }

    if (!this.hasCorrectProcedureCount(baerCode)) {
        return [false, 'invalid-procedure-count']
    }

    // Here we check the validity time for negative tests
    if (defaults.TYPE_OF_PROCEDURE.test.includes(procedure.type) && baerCode.procedure_result === false) {
        const procedureTypeMaxAge = defaults.AVAILABLE_PROCEDURES[procedure.type] + '_max_age'

        return this.negativeTestAgeValid(
            currentTime,
            configAndBaerCode.config[procedureTypeMaxAge],
            procedure.time)

        // Here we check the validity time people who have recovered from COVID-19.
    } else if (procedure.type === 7 && baerCode.procedure_result === true) {
        return this.positivePcrTestAgeValid(currentTime, procedure.time)

        // Here we check if procedures match to classify BärCODE as "fully vaccinated".
    } else if (defaults.TYPE_OF_PROCEDURE.vaccine.includes(procedure.type)) {
        return this.vaccineWithinInterval(
            currentTime,
            configAndBaerCode.config.vaccine_min_age,
            configAndBaerCode.config.vaccine_max_age,
            procedure.time)
    } else {
        return [false, 'procedure-missing-contact-support']
    }
}

baercodeVerifier.hasCorrectProcedureCount = function (baerCode) {
    const lastProcedure = this.getLatestProcedure(baerCode)
    const firstProcedure = this.getEarliestProcedure(baerCode)
    let countMatches = false
    let typeSumMatches = true

    if (defaults.TYPE_OF_PROCEDURE.vaccine.includes(lastProcedure.type)) {
        if (!baerCode.procedures.every(p => defaults.TYPE_OF_PROCEDURE.vaccine.includes(p.type))) {
            return false
        }
        countMatches = baerCode.procedures.length === defaults.PROCEDURE_REQUIRED_COUNT[firstProcedure.type]
        const typeSum = baerCode.procedures.filter(p => defaults.TYPE_OF_PROCEDURE.vaccine.includes(p.type)).length
        typeSumMatches = typeSum === defaults.PROCEDURE_REQUIRED_COUNT[firstProcedure.type]
    } else {
        countMatches = baerCode.procedures.length === defaults.PROCEDURE_REQUIRED_COUNT[lastProcedure.type]
    }

    if (countMatches && typeSumMatches) {
        return true
    } else {
        return false
    }
}

baercodeVerifier.hasMatchingCredentialType = function (credType, procedureType) {
    if (!Object.keys(defaults.CREDENTIAL_TYPE).includes(credType.toString())) {
        return [false, 'credential-type-not-found']
    }

    const credentialType = defaults.CREDENTIAL_TYPE[credType]
    const procedureTypes = defaults.TYPE_OF_PROCEDURE[credentialType]

    if (procedureTypes.includes(procedureType)) {
        return [true, 'credential-type-matches']
    } else {
        return [false, 'credential-type-mismatch']
    }
}

module.exports = baercodeVerifier
