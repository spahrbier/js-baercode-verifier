/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

const verifierConfiguration = {}

const defaults = require('./defaults')

const antigenCheckbox = document.getElementById('antigenCheckbox')
const antigenRange = document.getElementById('antigen-range')
const pcrCheckbox = document.getElementById('pcrCheckbox')
const pcrRange = document.getElementById('pcr-range')
const vaccineCheckbox = document.getElementById('vaccineCheckbox')
const vaccineMinRange = document.getElementById('vaccine-min-range')
const vaccineMaxRange = document.getElementById('vaccine-max-range')
const recoveredCheckbox = document.getElementById('recoveredCheckbox')
const recoveredMinRange = document.getElementById('recovered-min-range')
const recoveredMaxRange = document.getElementById('recovered-max-range')
const playSoundCheckbox = document.getElementById('playSoundCheckbox')

const checkboxes = [
    antigenCheckbox,
    pcrCheckbox,
    vaccineCheckbox,
    recoveredCheckbox
]
const ranges = {
    antigen_max_age: antigenRange,
    pcr_max_age: pcrRange,
    vaccine_min_age: vaccineMinRange,
    vaccine_max_age: vaccineMaxRange,
    recovered_min_age_days: recoveredMinRange,
    recovered_max_age_days: recoveredMaxRange
}
const spans = {
    antigen_max_age: document.getElementById('antigen-range-val'),
    pcr_max_age: document.getElementById('pcr-range-val'),
    vaccine_min_age: document.getElementById('vaccine-min-range-val'),
    vaccine_max_age: document.getElementById('vaccine-max-range-val'),
    recovered_min_age_days: document.getElementById('recovered-min-range-val'),
    recovered_max_age_days: document.getElementById('recovered-max-range-val')
}

let verifierConfig = { ...defaults.DEFAULT_CONFIG }
let scannedConfig = { ...defaults.DEFAULT_CONFIG }

verifierConfiguration.getConfig = function () {
    return { ...verifierConfig }
}

verifierConfiguration.getScannedConfig = function () {
    return { ...scannedConfig }
}

verifierConfiguration.extractDataFromConfigModal = function () {
    const acceptedProcedures = []

    checkboxes.forEach(function (checkbox, index, array) {
        checkbox.checked && acceptedProcedures.push(parseInt(checkbox.value))
    })

    const data = {
        accepted_procedures: acceptedProcedures
    }

    for (const [key, value] of Object.entries(ranges)) {
        data[key] = parseInt(value.value)
    }

    data.play_sound = playSoundCheckbox.checked

    return data
}

verifierConfiguration.setDataInConfigModal = function (configData) {
    checkboxes.forEach(function (checkbox, index, array) {
        const testId = index + 1
        checkbox.checked = configData.accepted_procedures.includes(testId)
    })

    for (const [key, value] of Object.entries(ranges)) {
        const stringValue = '' + configData[key]
        value.value = stringValue
        spans[key].innerText = stringValue
    }

    playSoundCheckbox.checked = configData.play_sound
}

verifierConfiguration.getConfigurationStrings = function (configData, addHtml = false) {
    let resultString = ''
    let prefix = '<p>'
    let suffix = '</p>\n'

    if (!addHtml) {
        prefix = ''
        suffix = '\n'
    }

    const acceptedProcedures = []
    configData.accepted_procedures.forEach(function (procedure, index, array) {
        acceptedProcedures.push($.i18n(defaults.AVAILABLE_PROCEDURES_I18N[procedure]))
    })

    resultString = resultString + prefix + $.i18n('scanned-config-allowed-procedures') + acceptedProcedures.join(', ') + suffix

    for (const [key, value] of Object.entries(defaults.PROCEDURE_MAPPINGS)) {
        if (configData.accepted_procedures.includes(parseInt(key))) {
            value.forEach(x => {
                const stringValue = prefix + $.i18n(defaults.PROCEDURE_I18N_MAPPINGS[x]) + ' ' + configData[x] + suffix
                resultString = resultString + stringValue
            })
        }
    }

    const playSoundState = configData.play_sound ? 'on' : 'off'
    resultString = resultString + prefix + $.i18n('play-sound-label') + ' ' + $.i18n('play-sound-' + playSoundState) + suffix

    return resultString
}

verifierConfiguration.setConfigModalFromStoredSettings = function () {
    this.setDataInConfigModal(verifierConfig)
}

verifierConfiguration.storeScannedConfiguration = function (data) {
    const [valid, configData, errors] = this.validateConfig(data)

    // TODO: Add checks for values that should not overlap and such.

    if (valid) {
        scannedConfig = data
        console.log('Updated scanned configuration:' + data)
    } else {
        console.log('Invalid configuration:' + data)
    }

    return [valid, configData, errors]
}

verifierConfiguration.storeConfiguration = function (data) {
    const [valid, configData, errors] = this.validateConfig(data)

    // TODO: Add checks for values that should not overlap and such.

    if (valid) {
        verifierConfig = data
        console.log('Updated configuration:' + configData)
    } else {
        console.log('Invalid configuration:' + configData)
    }

    return [valid, configData, errors]
}

verifierConfiguration.setConfigFromScanned = function () {
    console.log('Updating configuration from scanned data.')
    verifierConfig = { ...scannedConfig }
}

verifierConfiguration.storeModalConfiguration = function () {
    const modalData = this.extractDataFromConfigModal()
    return this.storeConfiguration(modalData)
}

verifierConfiguration.validateConfig = function (jsonObject) {
    const valid = defaults.configSchemaValidator(jsonObject)
    return [valid, valid ? jsonObject : null, defaults.configSchemaValidator.errors]
}

verifierConfiguration.parseAndValidateConfig = function (jsonString) {
    return this.validateConfig(JSON.parse(jsonString))
}

verifierConfiguration.getAcceptedProcedures = function (configData) {
    let acceptedProcedures = []

    const mapping = {
        1: [1],
        2: [2],
        3: defaults.TYPE_OF_PROCEDURE.vaccine
    }
    configData.accepted_procedures.forEach(p => {
        acceptedProcedures = acceptedProcedures.concat(mapping[p.toString()])
    })

    return acceptedProcedures
}

module.exports = verifierConfiguration
