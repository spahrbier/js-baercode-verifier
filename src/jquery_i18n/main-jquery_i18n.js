import fs from 'fs'
const updateTexts = function () { $('body').i18n() }
const translations = {
  en: JSON.parse(fs.readFileSync(__dirname + '/i18n/en.json', 'utf8')),
  de: JSON.parse(fs.readFileSync(__dirname + '/i18n/de.json', 'utf8')),
  se: JSON.parse(fs.readFileSync(__dirname + '/i18n/se.json', 'utf8'))
}

jQuery(document).ready(function () {
  $.i18n().load(translations).done(function () {
    $.i18n().locale = 'de'
    console.log('Loading translations done!')
  })

  updateTexts()
})

$('.lang-switch').click(function (e) {
  e.preventDefault()
  $.i18n().locale = $(this).data('locale')
  updateTexts()
})
