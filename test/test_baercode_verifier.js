/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require('jsdom-global')()
const assert = require('assert')
const defaults = require('../src/defaults')
const baercodeVerifier = require('../src/baercode_verifier')

const TIME_NOW = new Date('2021-04-19T07:15:00')
const defaultBaercode = function () {
    return {
        first_name: 'Max',
        last_name: 'Mustermann',
        date_of_birth: new Date('1990-01-02'),
        disease_type: 1,
        procedures: [{
            type: 4,
            time: new Date('1977-09-05')
        }],
        procedure_operator: 'Acme GmbH',
        procedure_result: false
    }
}

const defaultProcedure = function () {
    return {
        type: 1,
        time: new Date('1977-09-05')
    }
}

const changeProcedureType = function (type) {
    const procedure = defaultProcedure()
    procedure.type = type
    return [procedure]
}

const changeProcedureTime = function (time) {
    const procedure = defaultProcedure()
    procedure.time = time
    return [procedure]
}

const changeProcedureTypeTime = function (type, time) {
    const procedure = defaultProcedure()
    procedure.type = type
    procedure.time = time
    return [procedure]
}

const DEFAULT_CONFIG = { ...defaults.DEFAULT_CONFIG }
const TEN_SECONDS = 10000

describe('Check validity of BärCODE data', function () {
    describe('procedureAllowed() validates allowed procedure types', function () {
        it('allows a configured procedure', function () {
            const [valid, message] = baercodeVerifier.procedureAllowed([1, 2, 3], 2)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-allowed')
        })
        it('it disallows a non-existent procedure', function () {
            const [valid, message] = baercodeVerifier.procedureAllowed([1, 2, 3], 65535)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-not-allowed')
        })
    })

    describe('vaccineWithinInterval() validates vaccine time span', function () {
        const vaccineBaerCode = defaultBaercode()
        vaccineBaerCode.procedures = changeProcedureType(4)
        it('accepts a vaccination within configured span (10s from start)', function () {
            const procedureTime = new Date(TIME_NOW.getTime() - (2 * 7 * 24 * 60 * 60 * 1000) - TEN_SECONDS)
            const [valid, message] = baercodeVerifier.vaccineWithinInterval(
                TIME_NOW,
                2,
                52,
                procedureTime)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-within-range')
        })
        it('accepts a vaccination within configured span (10s from end)', function () {
            const timeNowFuture = new Date(TIME_NOW.getTime() + (52 * 7 * 24 * 60 * 60 * 1000) - TEN_SECONDS)
            const [valid, message] = baercodeVerifier.vaccineWithinInterval(
                timeNowFuture,
                2,
                52,
                TIME_NOW)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-within-range')
        })
        it('has to be a minimum of 2 weeks old', function () {
            const procedureTime = new Date(TIME_NOW.getTime() - (14 * 24 * 60 * 60 * 1000) + TEN_SECONDS)
            const [valid, message] = baercodeVerifier.vaccineWithinInterval(
                TIME_NOW,
                2,
                52,
                procedureTime)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-age-below-minimum')
        })
        it('has to be a maximum of 52 weeks old', function () {
            const timeNowFuture = new Date(TIME_NOW.getTime() + (52 * 7 * 24 * 60 * 60 * 1000) + TEN_SECONDS)
            const [valid, message] = baercodeVerifier.vaccineWithinInterval(
                timeNowFuture,
                2,
                52,
                TIME_NOW)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-age-above-maximum')
        })
        it('rejects a vaccination date in the future', function () {
            const procedureTime = new Date(TIME_NOW.getTime() + TEN_SECONDS)
            const [valid, message] = baercodeVerifier.vaccineWithinInterval(
                TIME_NOW,
                2,
                52,
                procedureTime)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-time-in-future')
        })
    })

    describe('isRecoveredAndVaccinated() validates if there is a valid combination of vaccination and recovered', function () {
        it('validates recovered and vaccination combinations', function () {
            const testExpectations = [
                {
                    procedures: [{
                        type: 3,
                        time: new Date('2063-04-05')
                    }, {
                        type: defaults.RECOVERED_PROCEDURE,
                        time: new Date('2062-12-24')
                    }],
                    valid: true,
                    message: 'is-recovered-and-vaccinated'
                },
                {
                    procedures: [{
                        type: defaults.RECOVERED_PROCEDURE,
                        time: new Date('2063-04-05')
                    }, {
                        type: 3,
                        time: new Date('2062-12-24')
                    }],
                    valid: false,
                    message: 'is-not-recovered-and-vaccinated'
                },
                {
                    procedures: [{
                        type: 3,
                        time: new Date('2063-04-05')
                    }, {
                        type: 3,
                        time: new Date('2062-12-24')
                    }],
                    valid: false,
                    message: 'is-not-recovered-and-vaccinated'
                },
                {
                    procedures: [{
                        type: defaults.RECOVERED_PROCEDURE,
                        time: new Date('2063-04-05')
                    }, {
                        type: defaults.RECOVERED_PROCEDURE,
                        time: new Date('2062-12-24')
                    }],
                    valid: false,
                    message: 'is-not-recovered-and-vaccinated'
                }
            ]
            const excludeRecovered = defaults.TYPE_OF_PROCEDURE.vaccine.filter(x => x !== defaults.RECOVERED_PROCEDURE)
            excludeRecovered.forEach(function (procedure, index, array) {
                testExpectations.push({
                    procedures: [{
                        type: procedure,
                        time: new Date('2063-04-05')
                    }, {
                        type: defaults.RECOVERED_PROCEDURE,
                        time: new Date('2062-12-24')
                    }],
                    valid: true,
                    message: 'is-recovered-and-vaccinated'
                })
            })

            testExpectations.forEach(function (testData, index, array) {
                const baerCode = defaultBaercode()
                baerCode.procedures = testData.procedures
                const [isValid, message] = baercodeVerifier.isRecoveredAndVaccinated(baerCode)
                assert.strictEqual(isValid, testData.valid)
                assert.strictEqual(message, testData.message)
            })
        })
    })

    describe('negativeTestAgeValid() validates age of test', function () {
        it('accepts a test that is newer than maximum allowed age', function () {
            const procedureTime = new Date(TIME_NOW.getTime() - (24 * 60 * 60 * 1000) + TEN_SECONDS)
            const [valid, message] = baercodeVerifier.negativeTestAgeValid(TIME_NOW, 24, procedureTime)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-valid')
        })
        it('rejects a test that is older than maximum allowed age', function () {
            const procedureTime = new Date(TIME_NOW.getTime() - (24 * 60 * 60 * 1000) - TEN_SECONDS)
            const [valid, message] = baercodeVerifier.negativeTestAgeValid(TIME_NOW, 24, procedureTime)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-validity-expired')
        })
        it('it rejects a test in the future', function () {
            const procedureTime = new Date(TIME_NOW.getTime() + TEN_SECONDS)
            const [valid, message] = baercodeVerifier.negativeTestAgeValid(TIME_NOW, 24, procedureTime)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-time-in-future')
        })
    })

    describe('positivePcrTestAgeValid() validates age of test', function () {
        it('accepts a test that is newer than maximum allowed age', function () {
            const procedureTime = new Date(
                TIME_NOW.getTime() - (defaults.DEFAULT_CONFIG.recovered_max_age_days * 24 * 60 * 60 * 1000) + TEN_SECONDS
            )
            const [valid, message] = baercodeVerifier.positivePcrTestAgeValid(TIME_NOW, procedureTime)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-valid')
        })
        it('rejects a test that is older than maximum allowed age', function () {
            const procedureTime = new Date(
                TIME_NOW.getTime() - (defaults.DEFAULT_CONFIG.recovered_max_age_days * 24 * 60 * 60 * 1000) - TEN_SECONDS
            )
            const [valid, message] = baercodeVerifier.positivePcrTestAgeValid(TIME_NOW, procedureTime)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-validity-expired')
        })
        it('accepts a test that is older than minimum allowed age', function () {
            const procedureTime = new Date(
                TIME_NOW.getTime() - (defaults.DEFAULT_CONFIG.recovered_min_age_days * 24 * 60 * 60 * 1000) - TEN_SECONDS
            )
            const [valid, message] = baercodeVerifier.positivePcrTestAgeValid(TIME_NOW, procedureTime)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-valid')
        })
        it('rejects a test that is newer than minimum allowed age', function () {
            const procedureTime = new Date(
                TIME_NOW.getTime() - (defaults.DEFAULT_CONFIG.recovered_min_age_days * 24 * 60 * 60 * 1000) + TEN_SECONDS
            )
            const [valid, message] = baercodeVerifier.positivePcrTestAgeValid(TIME_NOW, procedureTime)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-validity-expired')
        })
        it('it rejects a test in the future', function () {
            const procedureTime = new Date(TIME_NOW.getTime() + TEN_SECONDS)
            const [valid, message] = baercodeVerifier.positivePcrTestAgeValid(TIME_NOW, procedureTime)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-time-in-future')
        })
    })

    describe('hasCorrectProcedureCount() validates that the procedure type required count matches', function () {
        const baerCode = defaultBaercode()
        it('accepts a BärCODE with correct procedure count', function () {
            for (const [procedureType, count] of Object.entries(defaults.PROCEDURE_REQUIRED_COUNT)) {
                const procedures = []
                for (let i = 0; i < count; i++) {
                    procedures.push({
                        type: procedureType,
                        time: new Date(baercodeVerifier.getLatestProcedure(baerCode).time.getTime() + i)
                    })
                    baerCode.procedures = procedures
                }
                assert.strictEqual(baercodeVerifier.hasCorrectProcedureCount(baerCode), true)
            }
        })
        it('rejects a BärCODE with incorrect procedure count', function () {
            for (const [procedureType, count] of Object.entries(defaults.PROCEDURE_REQUIRED_COUNT)) {
                const procedures = []
                for (let i = 0; i <= count; i++) {
                    procedures.push({
                        type: procedureType,
                        time: new Date(baercodeVerifier.getLatestProcedure(baerCode).time.getTime() + i)
                    })
                    baerCode.procedures = procedures
                }
                assert.strictEqual(baercodeVerifier.hasCorrectProcedureCount(baerCode), false)
            }
        })
        it('rejects a BärCODE where count is correct, but procedure class do not match', function () {
            baerCode.procedures = [
                {
                    type: 3,
                    time: new Date('2021-02-01')
                },
                {
                    type: 1,
                    time: new Date('2021-01-01')
                }
            ]
            assert.strictEqual(baercodeVerifier.hasCorrectProcedureCount(baerCode), false)
        })
        it('accepts a BärCODE with count and class mismatch, lower count on latest procedure', function () {
            baerCode.procedures = [
                {
                    type: 4,
                    time: new Date('2021-02-01')
                },
                {
                    type: 3,
                    time: new Date('2021-01-01')
                }
            ]
            assert.strictEqual(baercodeVerifier.hasCorrectProcedureCount(baerCode), true)
        })

        // FIXME: Is this a fixme or not? This situtation should not occur, right?
        it('rejects a BärCODE with count and class mismatch, lower on first procedure', function () {
            baerCode.procedures = [
                {
                    type: 3,
                    time: new Date('2021-02-01')
                },
                {
                    type: 4,
                    time: new Date('2021-01-01')
                }
            ]
            assert.strictEqual(baercodeVerifier.hasCorrectProcedureCount(baerCode), false)
        })
    })

    describe('hasMatchingCredentialType() checks that BärCODE procecure type matches credential type', function () {
        it('it accepts tests with test credential type', function () {
            defaults.TYPE_OF_PROCEDURE.test.forEach(type => {
                const [valid, message] = baercodeVerifier.hasMatchingCredentialType(2, type)
                assert.strictEqual(valid, true)
                assert.strictEqual(message, 'credential-type-matches')
            })
        })
        it('it accepts vaccinations with vaccine credential type', function () {
            defaults.TYPE_OF_PROCEDURE.vaccine.forEach(type => {
                const [valid, message] = baercodeVerifier.hasMatchingCredentialType(1, type)
                assert.strictEqual(valid, true)
                assert.strictEqual(message, 'credential-type-matches')
            })
        })
        it('it rejects a test with vaccination credential type', function () {
            const [valid, message] = baercodeVerifier.hasMatchingCredentialType(1, 1)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'credential-type-mismatch')
        })
        it('it rejects a vaccination with test credential type', function () {
            const [valid, message] = baercodeVerifier.hasMatchingCredentialType(2, 3)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'credential-type-mismatch')
        })
        it('it rejects a BärCODE with invalid credential type', function () {
            const [valid, message] = baercodeVerifier.hasMatchingCredentialType(3, 3)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'credential-type-not-found')
        })
    })
    describe('baerCodeValid() wraps all validation functions for shorthand usage', function () {
        const validTestTime = new Date(TIME_NOW.getTime() - (12 * 60 * 60 * 1000))

        it('allows a configured procedure', function () {
            const baerCode = defaultBaercode()
            baerCode.procedures = changeProcedureTime(validTestTime)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 2, baerCode)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-valid')
        })
        it('it disallows an unconfigured procedure', function () {
            const config = { ...DEFAULT_CONFIG }
            config.accepted_procedures = [1, 2]
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, config, 1, defaultBaercode())
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-not-allowed')
        })
        it('it disallows a non-existent procedure', function () {
            const baerCode = defaultBaercode()
            baerCode.procedures = changeProcedureType(65535)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'invalid-baercode')
        })
        it('it rejects a BärCODE where procedure type does not match credential type', function () {
            const baerCode = defaultBaercode()
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 2, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'credential-type-mismatch')
        })
        it('accepts a vaccination within configured span (10s from start)', function () {
            const baerCode = defaultBaercode()
            const tensecs = new Date(TIME_NOW.getTime() - (2 * 7 * 24 * 60 * 60 * 1000) - TEN_SECONDS)
            baerCode.procedures = changeProcedureTypeTime(4, tensecs)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-within-range')
        })
        it('accepts a vaccination within configured span (10s from end)', function () {
            const baerCode = defaultBaercode()
            const timeNowFuture = new Date(TIME_NOW.getTime() + (52 * 7 * 24 * 60 * 60 * 1000) - TEN_SECONDS)
            baerCode.procedures = changeProcedureTypeTime(4, TIME_NOW)
            const [valid, message] = baercodeVerifier.baerCodeValid(timeNowFuture, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-within-range')
        })

        it('rejects a vaccination with incorrect amount of procedures', function () {
            const baerCode = defaultBaercode()
            const timeNowFuture = new Date(TIME_NOW.getTime() + (52 * 7 * 24 * 60 * 60 * 1000) - TEN_SECONDS)
            baerCode.procedures = [
                {
                    type: 3,
                    time: TIME_NOW
                }
            ]
            const [valid, message] = baercodeVerifier.baerCodeValid(timeNowFuture, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'invalid-procedure-count')
        })
        it('rejects a vaccination with not matching procedures types', function () {
            const baerCode = defaultBaercode()
            const timeNowFuture = new Date(TIME_NOW.getTime() + (52 * 7 * 24 * 60 * 60 * 1000) - TEN_SECONDS)
            baerCode.procedures = [
                {
                    type: 3,
                    time: TIME_NOW
                },
                {
                    type: 1,
                    time: TIME_NOW
                }
            ]
            const [valid, message] = baercodeVerifier.baerCodeValid(timeNowFuture, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'invalid-procedure-count')
        })
        it('vaccination has to be a minimum of 2 weeks old', function () {
            const baerCode = defaultBaercode()
            const ptime = new Date(TIME_NOW.getTime() - (14 * 24 * 60 * 60 * 1000) + TEN_SECONDS)
            baerCode.procedures = changeProcedureTypeTime(4, ptime)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-age-below-minimum')
        })
        it('vaccination has to be a maximum of 52 weeks old', function () {
            const baerCode = defaultBaercode()
            const timeNowFuture = new Date(TIME_NOW.getTime() + (52 * 7 * 24 * 60 * 60 * 1000) + TEN_SECONDS)
            baerCode.procedures = changeProcedureTypeTime(4, TIME_NOW)
            const [valid, message] = baercodeVerifier.baerCodeValid(timeNowFuture, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-age-above-maximum')
        })
        it('rejects a vaccination date in the future', function () {
            const baerCode = defaultBaercode()
            const ftime = new Date(TIME_NOW.getTime() + TEN_SECONDS)
            baerCode.procedures = changeProcedureTypeTime(4, ftime)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-time-in-future')
        })
        it('accepts a recovered + vaccination BärCODE within configured span (10s from start)', function () {
            const baerCode = defaultBaercode()
            baerCode.procedures = [{
                type: 3,
                time: new Date(TIME_NOW.getTime() - (2 * 7 * 24 * 60 * 60 * 1000) - TEN_SECONDS)
            }, {
                type: defaults.RECOVERED_PROCEDURE,
                time: new Date(TIME_NOW.getTime() - (180 * 24 * 60 * 60 * 1000))
            }]
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-within-range')
        })
        it('rejects a recovered + vaccination BärCODE with only recovered', function () {
            const baerCode = defaultBaercode()
            baerCode.procedures = [{
                type: defaults.RECOVERED_PROCEDURE,
                time: new Date(TIME_NOW.getTime() - (2 * 7 * 24 * 60 * 60 * 1000) - TEN_SECONDS)
            }, {
                type: defaults.RECOVERED_PROCEDURE,
                time: new Date(TIME_NOW.getTime() - (180 * 24 * 60 * 60 * 1000))
            }]
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'invalid-procedure-count')
        })
        it('accepts an antigen test that is newer than maximum allowed age', function () {
            const baerCode = defaultBaercode()
            const ntime = new Date(TIME_NOW.getTime() - (24 * 60 * 60 * 1000) + TEN_SECONDS)
            baerCode.procedures = changeProcedureTime(ntime)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 2, baerCode)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-valid')
        })
        it('rejects antigen test that is older than maximum allowed age', function () {
            const baerCode = defaultBaercode()
            const otime = new Date(TIME_NOW.getTime() - (24 * 60 * 60 * 1000) - TEN_SECONDS)
            baerCode.procedures = changeProcedureTime(otime)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 2, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-validity-expired')
        })
        it('it rejects an antigen test in the future', function () {
            const baerCode = defaultBaercode()
            const ftime = new Date(TIME_NOW.getTime() + TEN_SECONDS)
            baerCode.procedures = changeProcedureTime(ftime)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 2, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-time-in-future')
        })
        it('accepts a negative PCR test that is newer than maximum allowed age', function () {
            const baerCode = defaultBaercode()
            const ntime = new Date(TIME_NOW.getTime() - (72 * 60 * 60 * 1000) + TEN_SECONDS)
            baerCode.procedures = changeProcedureTypeTime(2, ntime)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 2, baerCode)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-valid')
        })
        it('rejects a negative PCR test that is older than maximum allowed age', function () {
            const baerCode = defaultBaercode()
            const otime = new Date(TIME_NOW.getTime() - (72 * 60 * 60 * 1000) - TEN_SECONDS)
            baerCode.procedures = changeProcedureTypeTime(2, otime)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 2, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-validity-expired')
        })
        it('it rejects a negative PCR test in the future', function () {
            const baerCode = defaultBaercode()
            const ftime = new Date(TIME_NOW.getTime() + TEN_SECONDS)
            baerCode.procedures = changeProcedureTypeTime(2, ftime)
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 2, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-time-in-future')
        })
        it('accepts a positive PCR test that is newer than maximum allowed age', function () {
            const baerCode = defaultBaercode()
            const ntime = new Date(
                TIME_NOW.getTime() - (defaults.DEFAULT_CONFIG.recovered_max_age_days * 24 * 60 * 60 * 1000) + TEN_SECONDS
            )
            baerCode.procedures = changeProcedureTypeTime(7, ntime)
            baerCode.procedure_result = true
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-valid')
        })
        it('rejects a positive PCR test that is older than maximum allowed age', function () {
            const baerCode = defaultBaercode()
            const otime = new Date(
                TIME_NOW.getTime() - (defaults.DEFAULT_CONFIG.recovered_max_age_days * 24 * 60 * 60 * 1000) - TEN_SECONDS
            )
            baerCode.procedures = changeProcedureTypeTime(7, otime)
            baerCode.procedure_result = true
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-validity-expired')
        })
        it('accepts a positive PCR test that is older than minimum allowed age', function () {
            const baerCode = defaultBaercode()
            const ntime = new Date(
                TIME_NOW.getTime() - (defaults.DEFAULT_CONFIG.recovered_min_age_days * 24 * 60 * 60 * 1000) - TEN_SECONDS
            )
            baerCode.procedures = changeProcedureTypeTime(7, ntime)
            baerCode.procedure_result = true
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, true)
            assert.strictEqual(message, 'procedure-age-valid')
        })
        it('rejects a positive PCR test that is newer than minimum allowed age', function () {
            const baerCode = defaultBaercode()
            const otime = new Date(
                TIME_NOW.getTime() - (defaults.DEFAULT_CONFIG.recovered_min_age_days * 24 * 60 * 60 * 1000) + TEN_SECONDS
            )
            baerCode.procedures = changeProcedureTypeTime(7, otime)
            baerCode.procedure_result = true
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-validity-expired')
        })
        it('it rejects a positive PCR test in the future', function () {
            const baerCode = defaultBaercode()
            const ftime = new Date(TIME_NOW.getTime() + TEN_SECONDS)
            baerCode.procedures = changeProcedureTypeTime(7, ftime)
            baerCode.procedure_result = true
            const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, DEFAULT_CONFIG, 1, baerCode)
            assert.strictEqual(valid, false)
            assert.strictEqual(message, 'procedure-time-in-future')
        })
    })
})
